{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "7PAvAsqN-NrB"
   },
   "source": [
    "# Image segmentation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Terminology\n",
    "\n",
    "- **semantic** segmentation: division of an image into meaningful parts\n",
    "- **instance** segmentation: division of an image into objects\n",
    "\n",
    "Example:\n",
    "\n",
    "<img src=\"illustrations/semantic_instance.png\" alt=\"drawing\" width=\"80%\" class=\"center\"/>\n",
    "\n",
    "<sup>Source: \"Introduction to Bioimage Analysis\" by Pete Bankhead [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)</sup>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Semantic segmentation\n",
    "\n",
    "- **Goal**: assign a class to each pixel in an image\n",
    "- **Input**: image\n",
    "- **Output**: image with the same size as the input, where each pixel is assigned a class\n",
    "\n",
    "Methods:\n",
    "- classical image processing:\n",
    "    - thresholding (+ prefiltering)\n",
    "    - edge detection\n",
    "    - region growing\n",
    "    - active contours (snakes)\n",
    "    - ...\n",
    "- classification using machine learning (see day 3):\n",
    "    - random forests (e.g. using *Ilastik* or *Weka* in Fiji)\n",
    "    - clustering (e.g. in intensity space)\n",
    "    - ...\n",
    "    - convolutional neural networks, e.g. *U-Net* (see day 3)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Instance segmentation (or object segmentation)\n",
    "\n",
    "- **Goal**: detect and segment individual objects in an image\n",
    "- **Input**: image\n",
    "- **Output**: image with the same size as the input, where each pixel is assigned an instance\n",
    "\n",
    "Methods:\n",
    "- classical image processing:\n",
    "    - connected components\n",
    "    - watershed (or flooding)\n",
    "    - non-maximum suppression\n",
    "- machine learning:\n",
    "    - clustering\n",
    "    - typical approach: semantic segmentation + post-processing\n",
    "\n",
    "**Often derived from semantic segmentation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## From semantic to instance segmentation\n",
    "### Connected components\n",
    "\n",
    "Simplest case: objects are well separated\n",
    "\n",
    "Method of choice: connected components (or \"labeling\")\n",
    "\n",
    "<img src=\"illustrations/semantic_instance.png\" alt=\"drawing\" width=\"80%\" class=\"center\"/>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import skimage\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import stackview"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "im_blobs = skimage.io.imread('images/blobs.tif')\n",
    "im_th = im_blobs > skimage.filters.threshold_otsu(im_blobs)\n",
    "\n",
    "plt.imshow(im_th, cmap='gray')\n",
    "plt.colorbar()\n",
    "\n",
    "# stackview.picker(im_th.astype(np.uint8), colormap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels = skimage.morphology.label(im_th)\n",
    "\n",
    "plt.imshow(labels, cmap='gray', interpolation='nearest')\n",
    "plt.colorbar()\n",
    "\n",
    "# stackview.picker(labels.astype(np.uint8), colormap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## From semantic to instance segmentation\n",
    "### Watershed\n",
    "\n",
    "<img src=\"illustrations/watershed_mpl.gif\" alt=\"drawing\" width=\"60%\" class=\"center\"/>\n",
    "\n",
    "<sup>Source: \"PyImageCourse\" by Guillaume Witz</sup>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The distance transform\n",
    "\n",
    "<img src=\"illustrations/distance_transform.png\" alt=\"drawing\" width=\"70%\" class=\"center\"/>\n",
    "\n",
    "<sub>Illustration taken from \"Introduction to Bioimage Analysis\" by Pete Bankhead, [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).</sub>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import ndimage as ndimage\n",
    "\n",
    "dist = ndimage.distance_transform_edt(im_th)\n",
    "\n",
    "plt.imshow(dist, cmap='gray',)# interpolation='nearest')\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stackview.picker(dist[:100, :100], colormap='gray', zoom_factor=5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def threshold_im(image, sigma):\n",
    "    return image * (image > sigma)\n",
    "\n",
    "stackview.interact(threshold_im, dist[100:200, 50:200], zoom_factor=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finding markers for initialising the watershed segmentation\n",
    "\n",
    "Method 1: Eroding the semantic segmentation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "im_th_eroded = skimage.morphology.isotropic_erosion(im_th, radius=6)\n",
    "\n",
    "plt.imshow(im_th, cmap='gray')\n",
    "plt.imshow(im_th_eroded, cmap='Reds', alpha=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def erode_im(image, radius):\n",
    "    return skimage.morphology.isotropic_erosion(image, radius=radius)\n",
    "\n",
    "stackview.interact(erode_im, im_th, zoom_factor=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finding markers for initialising the watershed segmentation\n",
    "\n",
    "Method 2: Local extrema of the distance transform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# coords = skimage.feature.peak_local_max(dist, exclude_border=False)\n",
    "# coords = skimage.feature.peak_local_max(dist, min_distance=8, exclude_border=False)\n",
    "coords = skimage.feature.peak_local_max(\n",
    "    skimage.filters.gaussian(dist, 3), exclude_border=False)\n",
    "\n",
    "plt.imshow(dist, cmap='gray')\n",
    "# plt.imshow(im_blobs, cmap='gray')\n",
    "plt.scatter(coords[:, 1], coords[:, 0], s=5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "skimage.segmentation.watershed?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We need to create an image where the local maxima are the markers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "markers = np.zeros(im_th.shape, dtype='uint8')\n",
    "for i in range(len(coords)):\n",
    "    markers[coords[i, 0], coords[i, 1]] = i + 1\n",
    "\n",
    "plt.imshow(im_blobs)\n",
    "plt.imshow(markers, alpha=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels = skimage.segmentation.watershed(-dist, markers=markers, mask=im_th, watershed_line=True)\n",
    "\n",
    "plt.imshow(skimage.color.label2rgb(labels))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(dist, cmap='gray')\n",
    "plt.imshow(skimage.segmentation.find_boundaries(labels), alpha=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = skimage.segmentation.find_boundaries(labels)\n",
    "distv = dist.copy()\n",
    "distv[b] = 20\n",
    "\n",
    "stackview.interact(threshold_im, distv[100:200, 50:200], zoom_factor=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## From semantic to instance segmentation\n",
    "### Seeded watershed using edge information\n",
    "\n",
    "<img src=\"illustrations/watershed_blobs.gif\" alt=\"drawing\" width=\"50%\" class=\"center\"/>\n",
    "\n",
    "<sup>Source: imagej.net</sup>"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "collapsed_sections": [],
   "name": "09-Thresholding.ipynb",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.15"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
