{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "established-bidding",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# Building a pipeline and running data analysis\n",
    "\n",
    "- The present notebook demonstrates the sort of complete workflow that one can create with the tools seen before. It also shows how to export the extracted results and provides some visualization examples. \n",
    "\n",
    "\n",
    "- So that people already using Fiji macros have a reference point, this example follows the workflow demonstrated in the [NEUBIAS Academy course on Fiji Macro Programming](https://github.com/ahklemm/ImageJMacro_Introduction) given by Anna Klemm.  The example uses images from the [Cell Atlas](https://www.proteinatlas.org/humanproteome/cell) of the Human Protein Atlas (HPA) project where a large collection of proteins have been tagged and imaged to determine their cellular location. Specifically, in the following series of images we downloaded from the Atlas, we can see some cells showing nucleoplasm localization and some nuclear membrane localization. The idea of the workflow is to compare the signal within the nucleus with that on its edge to determine for each image whether the protein is membrane bound or not.\n",
    "\n",
    "\n",
    "<img src=\"illustrations/example_workflow_aim.png\" alt=\"drawing\" width=\"80%\" class=\"center\"/>\n",
    "\n",
    "\n",
    "*For more information  on the dataset, see the publication of Thul PJ et al., A subcellular map of the human proteome. **Science**. (2017) DOI: [10.1126/science.aal3321](https://doi.org/10.1126/science.aal3321). All images are covered by a [Creative Commons Attribution-ShareAlike 3.0 International License](https://creativecommons.org/licenses/by-sa/3.0/).*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a9f84d0",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Building a complete workflow\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5a52506-cfa2-4298-8edc-b85e7f49780d",
   "metadata": {},
   "source": [
    "First we import packages, load to images and display them to have a feeling of the content:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sharing-genealogy",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import skimage\n",
    "import skimage.io\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import skimage.filters\n",
    "from pathlib import Path\n",
    "import scipy.ndimage as ndi\n",
    "import pandas as pd\n",
    "from tqdm import tqdm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "casual-papua",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,2, figsize=(15,15))\n",
    "ax[0].imshow(skimage.io.imread('https://github.com/guiwitz/PyImageCourse_beginner/raw/cellatlas/images/24138_196_F7_2.tif'))\n",
    "ax[1].imshow(skimage.io.imread('https://github.com/guiwitz/PyImageCourse_beginner/raw/cellatlas/images/27897_273_C8_2.tif'));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "875e622c",
   "metadata": {},
   "source": [
    "Our pipeline will allow us to automatically find which proteins are enriched on the membrane. First we will build the analysis through a single image."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "exceptional-tragedy",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Import image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "accompanied-occurrence",
   "metadata": {},
   "outputs": [],
   "source": [
    "image = skimage.io.imread('https://github.com/guiwitz/PyImageCourse_beginner/raw/cellatlas/images/24138_196_F7_2.tif')\n",
    "plt.imshow(image);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "speaking-walnut",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Filter the image\n",
    "\n",
    "To obtain a smoother segmentation, we filter the nuclei images with a median filter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "welcome-parade",
   "metadata": {},
   "outputs": [],
   "source": [
    "im_filtered = skimage.filters.median(image[:,:,2], skimage.morphology.disk(5))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "common-premises",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(im_filtered);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "excessive-potter",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Automated threshold\n",
    "\n",
    "We now isolate the nuclei by setting an intensity threshold automatically using the Otsu method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "golden-compensation",
   "metadata": {},
   "outputs": [],
   "source": [
    "th = skimage.filters.threshold_otsu(im_filtered)\n",
    "nucl_th = im_filtered > th\n",
    "plt.imshow(nucl_th);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "convenient-symbol",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Clean-up with morphological operations\n",
    "\n",
    "We still have some debris and holes in the nuclei. We can use morphological operation to fix these problems:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "postal-ranch",
   "metadata": {},
   "outputs": [],
   "source": [
    "nucl_close = skimage.morphology.binary_closing(nucl_th, footprint=skimage.morphology.disk(5))\n",
    "nucl_fill = ndi.binary_fill_holes(nucl_close, skimage.morphology.disk(5))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "interpreted-capital",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(nucl_fill);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "retired-processing",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Compute a band around the nuclei\n",
    "\n",
    "Now we need to measure intensity in two different non-overlapping regions: inside the nuclei and on their border. To obtain the region **inside** the nuclei we simply erode their borders. Once we have such *smaller* nuclei, we can just subtract them from the original *larger* ones and obtain a band corresponding to the membrane. The size of this border depends on how much we eroded in the first place: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9f40b0c-f63d-412f-bf4d-e81684699cbd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your code here\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "alien-planner",
   "metadata": {
    "editable": true,
    "jupyter": {
     "source_hidden": true
    },
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Solution\n",
    "\n",
    "eroded = skimage.morphology.binary_erosion(nucl_fill, footprint=skimage.morphology.disk(10))\n",
    "edge = np.logical_and(nucl_fill, np.logical_not(eroded))\n",
    "\n",
    "plt.imshow(edge);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "harmful-copper",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Label nuclei and band and measure properties\n",
    "\n",
    "Now that we have pairs of binary masks, for nucleoplasm and nuclear membrane, we can label *connected* regions to measure them. We actually first label the *original* segmentation and only *then* mask those labels with the two binary segmentations in order to have the *same* labels for both, so that we can compare them and compute intensity ratios. Once we have labelled regions, we can measure for each element the ```area``` and ```mean_intensity``` in the protein channel:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d4b0732-4743-4e89-a952-8e47f1df34b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your code here\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "competitive-scroll",
   "metadata": {
    "editable": true,
    "jupyter": {
     "source_hidden": true
    },
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Solution\n",
    "\n",
    "im_lab = skimage.morphology.label(nucl_fill)\n",
    "measure_eroded = skimage.measure.regionprops_table(im_lab * eroded, image[:,:,1], properties=('label', 'mean_intensity', 'area'))\n",
    "measure_edge = skimage.measure.regionprops_table(im_lab * edge, image[:,:,1], properties=('label', 'mean_intensity', 'area'))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "moderate-angel",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Post-processing\n",
    "\n",
    "Finally, we want to store the measures in a dataframe and to compute the ratio of the intensity in the two segmentations (and store it into an other column)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b99d5f53-cee2-4a34-aa98-c8a5a78e9a60",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your code here\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aerial-sessions",
   "metadata": {
    "editable": true,
    "jupyter": {
     "source_hidden": true
    },
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Solution\n",
    "\n",
    "measure_eroded_df = pd.DataFrame(measure_eroded)\n",
    "measure_edge_df = pd.DataFrame(measure_edge)\n",
    "merged = pd.merge(measure_eroded_df, measure_edge_df, on='label', how='inner')\n",
    "merged['ratio'] = merged['mean_intensity_x']/merged['mean_intensity_y']\n",
    "merged.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1b16ec9-0361-4927-bce9-3ff9fc575c2c",
   "metadata": {
    "editable": true,
    "jupyter": {
     "source_hidden": true
    },
    "scrolled": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Solution\n",
    "\n",
    "merged['ratio'].hist();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd83df51-cfa1-498b-9f59-495a202f3273",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Creating a pipeline function"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b8fb099-16e7-44b3-9992-c4e351987698",
   "metadata": {},
   "source": [
    "We have seen previously how to define a function. It needs the ```def``` descriptor, a name and inputs. What we are doing here is just copying all the lines that we used for our analysis into this function. At the moment the only input is the path to a given file to analyze. We also try to document what the function does, what its input/output are etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7bd43394-0ba8-45ef-ac0e-8fb3a0ea2d4b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_pipeline(image_path):\n",
    "    \"\"\"\n",
    "    This function extracts information about nucleoplasm and membrane, and compute their intensity ratio for an image loaded at the given path\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    image_path: str\n",
    "        path to image\n",
    "        \n",
    "    Returns\n",
    "    -------\n",
    "    merged_df: pandas dataframe with label, area, mean_intensity, intensity ratio and extent information for each nucleus\n",
    "    \n",
    "    \"\"\"\n",
    "    \n",
    "    # import image\n",
    "    image = skimage.io.imread(image_path)\n",
    "\n",
    "    # define channels\n",
    "    channel_nuclei = 2\n",
    "    channel_signal = 1\n",
    "    \n",
    "    # filter the nuclei image\n",
    "    im_filtered = skimage.filters.median(image[:,:,channel_nuclei], skimage.morphology.disk(5))\n",
    "    \n",
    "    # threshold\n",
    "    th = skimage.filters.threshold_otsu(im_filtered)\n",
    "    nucl_th = im_filtered > th\n",
    "    \n",
    "    # clean-up\n",
    "    nucl_close = skimage.morphology.binary_closing(nucl_th, footprint=skimage.morphology.disk(5))\n",
    "    nucl_fill = ndi.binary_fill_holes(nucl_close, skimage.morphology.disk(5))\n",
    "    \n",
    "    # create band around nuclei\n",
    "    eroded = skimage.morphology.binary_erosion(nucl_fill, footprint=skimage.morphology.disk(10))\n",
    "    edge = np.logical_and(nucl_fill, np.logical_not(eroded))\n",
    "\n",
    "    # plot segmentation masks\n",
    "    fig, ax = plt.subplots(1, 2, figsize=(10,10))\n",
    "    ax[0].imshow(nucl_fill, cmap='gray')\n",
    "    ax[1].imshow(edge, cmap='gray')\n",
    "    ax[0].set_title('Nucleoplasm');\n",
    "    ax[1].set_title('Membrane');\n",
    "    \n",
    "    # label and measure\n",
    "    im_lab = skimage.morphology.label(nucl_fill)\n",
    "    measure_eroded = skimage.measure.regionprops_table(im_lab * eroded, image[:,:,channel_signal],\n",
    "                                                       properties=('label', 'mean_intensity', 'area'))\n",
    "    measure_edge = skimage.measure.regionprops_table(im_lab * edge, image[:,:,channel_signal],\n",
    "                                                     properties=('label', 'mean_intensity', 'area'))\n",
    "    # turn results into dataframes\n",
    "    measure_eroded_df = pd.DataFrame(measure_eroded)\n",
    "    measure_edge_df = pd.DataFrame(measure_edge)\n",
    "    # rename columns after merging dataframes to avoid confusions\n",
    "    measure_eroded_df.rename(columns={'mean_intensity': 'mean_intensity_nucleoplasm', 'area': 'area_nucleoplasm'}, inplace=True)\n",
    "    measure_edge_df.rename(columns={'mean_intensity': 'mean_intensity_membrane', 'area': 'area_membrane'}, inplace=True)\n",
    "    \n",
    "    # combined nuclei and band measures and compute intensity ratio mean\n",
    "    merged_df = pd.merge(measure_eroded_df, measure_edge_df, on='label', how='inner')\n",
    "    merged_df['ratio'] = merged_df['mean_intensity_nucleoplasm']/merged_df['mean_intensity_membrane']\n",
    "    \n",
    "    # store file name\n",
    "    merged_df['filename'] = os.path.basename(image_path)\n",
    "\n",
    "    return merged_df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63d01bd8-96ed-445b-9fb3-b2d602f3e252",
   "metadata": {},
   "source": [
    "We can now test our function with a single image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "078dcd70-56c8-49a8-9a9e-192941046d56",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_results = my_pipeline('https://github.com/guiwitz/PyImageCourse_beginner/raw/cellatlas/images/24138_196_F7_2.tif')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a591a85f-dbdd-4d8e-9383-198c73877310",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c1d7bfeb-e543-482a-8df0-26c7748c2b8c",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_results['ratio'].hist();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b186d2f-4eb2-40b7-b5d0-a1770f997e35",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Adjusting the function behavior\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31561b99-a5f5-468f-b69f-db5fb2c51c3c",
   "metadata": {},
   "source": [
    "We have recovered the same result as previously and can analyze again our data as we did before. The masks are also plotted by default when calling the function. This is helpful to test the function and verify that nothing went dramatically wrong but we probably don't want to see these images if we analyze hundreds of images. What we can do is leave the user the choice to see it or not. Let's adjust our function to do that: we add an additional **optional** parameter ```do_plotting``` which is simply a boolean (```True```/```False```). If True, plotting is happening, if False it's not."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70e05a01-48d0-4dd1-af7f-933f247ecf7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_pipeline(image_path, do_plotting=False):\n",
    "    \"\"\"\n",
    "    This function extracts information about nucleoplasm and membrane, and compute their intensity ratio for an image loaded at the given path\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    image_path: str\n",
    "        path to image\n",
    "    do_plotting: bool\n",
    "        show segmentation masks or not\n",
    "        \n",
    "    Returns\n",
    "    -------\n",
    "    final_df: pandas dataframe with label, area, mean_intensity, intensity ratio and extent information for each nucleus\n",
    "    \n",
    "    \"\"\"\n",
    "    \n",
    "    # import image\n",
    "    image = skimage.io.imread(image_path)\n",
    "\n",
    "    # define channels\n",
    "    channel_nuclei = 2\n",
    "    channel_signal = 1\n",
    "    \n",
    "    # filter the nuclei image\n",
    "    im_filtered = skimage.filters.median(image[:,:,channel_nuclei], skimage.morphology.disk(5))\n",
    "    \n",
    "    # threshold\n",
    "    th = skimage.filters.threshold_otsu(im_filtered)\n",
    "    nucl_th = im_filtered > th\n",
    "    \n",
    "    # clean-up\n",
    "    nucl_close = skimage.morphology.binary_closing(nucl_th, footprint=skimage.morphology.disk(5))\n",
    "    nucl_fill = ndi.binary_fill_holes(nucl_close, skimage.morphology.disk(5))\n",
    "    \n",
    "    # create band around nuclei\n",
    "    eroded = skimage.morphology.binary_erosion(nucl_fill, footprint=skimage.morphology.disk(10))\n",
    "    edge = np.logical_and(nucl_fill, np.logical_not(eroded))\n",
    "\n",
    "    if do_plotting:\n",
    "        # plot segmentation masks\n",
    "        fig, ax = plt.subplots(1, 2, figsize=(10,10))\n",
    "        ax[0].imshow(nucl_fill, cmap='gray')\n",
    "        ax[1].imshow(edge, cmap='gray')\n",
    "        ax[0].set_title('Nucleoplasm');\n",
    "        ax[1].set_title('Membrane');\n",
    "    \n",
    "    # label and measure\n",
    "    im_lab = skimage.morphology.label(nucl_fill)\n",
    "    measure_eroded = skimage.measure.regionprops_table(im_lab * eroded, image[:,:,channel_signal],\n",
    "                                                       properties=('label', 'mean_intensity', 'area'))\n",
    "    measure_edge = skimage.measure.regionprops_table(im_lab * edge, image[:,:,channel_signal],\n",
    "                                                     properties=('label', 'mean_intensity', 'area'))\n",
    "    # turn results into dataframes\n",
    "    measure_eroded_df = pd.DataFrame(measure_eroded)\n",
    "    measure_edge_df = pd.DataFrame(measure_edge)\n",
    "    # rename columns after merging dataframes to avoid confusions\n",
    "    measure_eroded_df.rename(columns={'mean_intensity': 'mean_intensity_nucleoplasm', 'area': 'area_nucleoplasm'}, inplace=True)\n",
    "    measure_edge_df.rename(columns={'mean_intensity': 'mean_intensity_membrane', 'area': 'area_membrane'}, inplace=True)\n",
    "    \n",
    "    # combined nuclei and band measures and compute intensity ratio mean\n",
    "    merged_df = pd.merge(measure_eroded_df, measure_edge_df, on='label', how='inner')\n",
    "    merged_df['ratio'] = merged_df['mean_intensity_nucleoplasm']/merged_df['mean_intensity_membrane']\n",
    "    \n",
    "    # store file name\n",
    "    merged_df['filename'] = os.path.basename(image_path)\n",
    "    \n",
    "    # filter out regions with nucleoplasm area < 100 pixels\n",
    "    final_df = merged_df[merged_df['area_nucleoplasm'] > 100].copy()\n",
    "\n",
    "    return final_df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de6c4b79-1e8c-49e6-8503-21e81a7ba32c",
   "metadata": {},
   "source": [
    "You see that we added an ```if``` statment in our code. This is the place where we control for plotting or not. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ef536c57-5a1c-4de8-9d9f-981e2fddb218",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_results = my_pipeline('https://github.com/guiwitz/PyImageCourse_beginner/raw/cellatlas/images/24138_196_F7_2.tif')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2fe26e71-1719-4f47-91c1-7c9dffe885a3",
   "metadata": {},
   "source": [
    "Now indeed our function doesn't produce any output if we don't explicitly ask for it. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6869506a-1d27-4ba1-b668-9fe67a4b9aeb",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Analyzing multiple images "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f74fb97e-3f85-4c30-8d78-21b92ad36e0a",
   "metadata": {},
   "source": [
    "The point of creating an image processing pipeline was to be able to easily analyze multiple images. We can do this now by using a simple for loop. We will iterate through all the images of our cells_atlas folder and store their paths in a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d180b493-4745-423d-a8b9-0092fb26eab9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Example with glob function (more compact synthax)\n",
    "images_folder = 'images/cells_atlas/'\n",
    "\n",
    "files_to_analyze = []\n",
    "\n",
    "for f in Path(images_folder).glob('*.tif'):\n",
    "    files_to_analyze.append(f)\n",
    "    print(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb238fcc-3e6b-418c-a2eb-ed0c23172689",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Example with os.listdir and .endswith function\n",
    "\n",
    "files_to_analyze = []\n",
    "for f in os.listdir(images_folder):\n",
    "    if f.lower().endswith('.tif'):\n",
    "        filepath = os.path.join(images_folder, f)\n",
    "        files_to_analyze.append(filepath)\n",
    "        print(filepath)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a28f9e1-5ad6-4b00-b371-982c69f7dc43",
   "metadata": {},
   "source": [
    "Now we create a ```for``` loop that will go through this list of files and analyze each of them with our function. Before the loop, we create an empty list that is going to be filled with the output of the function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41d7cc13-47d0-454c-a0c3-f678cee492a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_tables = []\n",
    "for file in files_to_analyze:\n",
    "    \n",
    "    # use the function\n",
    "    new_table = my_pipeline(file)\n",
    "    \n",
    "    # append the result to the list\n",
    "    all_tables.append(new_table)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9be382d5-c08e-4709-987c-450ab8d1a19a",
   "metadata": {},
   "source": [
    "We can even add a fancy progression bar to follow the progression of our pipeline and estimate the time it will take. We use here the [tqdm](https://tqdm.github.io) library. The synthax is as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dc083f89-4316-4282-9a19-6ab577c8fb99",
   "metadata": {},
   "outputs": [],
   "source": [
    "for file in tqdm(files_to_analyze):\n",
    "    \n",
    "    # use the function\n",
    "    new_table = my_pipeline(file)\n",
    "    \n",
    "    # append the result to the list\n",
    "    all_tables.append(new_table)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bc8b2b6-bb8a-4d94-b174-d83235e6b7b4",
   "metadata": {},
   "source": [
    "Finally we can concatenate our results into a single table. For this we use a Pandas function called ```concat``` which just glues two DataFrames together:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8ab5adfd-fc21-4537-bb3c-006c4810df34",
   "metadata": {},
   "outputs": [],
   "source": [
    "complete_results = pd.concat(all_tables)\n",
    "complete_results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a667098-e1fe-4d64-9fda-d6754a107001",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Data analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5b31ed3-5622-4b76-8869-9152fcb101c7",
   "metadata": {},
   "source": [
    "Now we can run data analysis on the results dataframe we got from our pipeline.\n",
    "If we go back to our ```complete_results``` data frame and plot an histogram of the ratios of intensities:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5cbbdd25-35f3-44ed-9bf4-322de7673bbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(complete_results['ratio']);\n",
    "plt.grid(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46d53ec7-1b76-4c1a-a5b5-5caecc34f07a",
   "metadata": {},
   "source": [
    "In this histogram, the intensity ratio for all segmented nuclei in all images are plotted. Imagine now that we just want to plot one mean value over nuclei for each image."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b67ff4d-12b1-4319-80b2-dbdc54aa89a3",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Exercise 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "120e6509-232c-4d6c-ba1f-a61366a441d4",
   "metadata": {},
   "source": [
    "1. Plot the histogram of mean intensity ratio over segmented nuclei for all images. Hint [here](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.groupby.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ae71edde-4a16-4e36-8389-593a16480d12",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write your code here\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "598ac3e8-8811-43e1-b67e-d74d1fafb5da",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Solution\n",
    "\n",
    "# group by 'filename' and compute the mean ratio for each file\n",
    "mean_ratios = complete_results.groupby('filename')['ratio'].mean().reset_index()\n",
    "\n",
    "# plot histogram of mean ratios\n",
    "plt.figure(figsize=(8, 6))\n",
    "plt.hist(mean_ratios['ratio'], bins=20, edgecolor='black')\n",
    "plt.title('Histogram of Mean Ratios per Image')\n",
    "plt.xlabel('Mean Ratio')\n",
    "plt.ylabel('Frequency')\n",
    "plt.grid(True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d562fedd-f2e3-455f-b745-064892e64257",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Visualizations with seaborn"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37bb84af-89d5-4369-aff4-6747f43d28a1",
   "metadata": {},
   "source": [
    "[Seaborn](https://seaborn.pydata.org) is a Python data visualization library based on matplotlib and is really simple to use with pandas dataframes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "591c1f25-be53-4406-8bba-c04bcdc93596",
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6656eb32-89f6-417d-bf11-c50bf7757f4f",
   "metadata": {},
   "source": [
    "There is a nice feature in seaborn named ```hue```. It is used to add a categorical variable (or sometimes a continuous variable) that differentiates data by color in various plots, such as histograms, scatter plots, bar plots, and more. When you specify the hue parameter, seaborn automatically assigns different colors to the categories in that variable, making it easy to compare distributions or trends across multiple groups on the same plot. Below are some examples of usage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "844d3b2d-f3b3-4f26-9f2e-fb370198f636",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot boxplots of ratios over different image files\n",
    "plt.figure(figsize=(8, 6))\n",
    "sns.boxplot(data=complete_results, x='ratio', hue='filename')\n",
    "plt.title('Boxplots of ratios per image')\n",
    "plt.xlabel('Ratio')\n",
    "plt.grid(True)\n",
    "plt.legend(title='File', bbox_to_anchor=(1.05, 1), loc='upper left', ncol=1)\n",
    "plt.tight_layout(rect=[0, 0, 1, 1])  # Adjust the right side of the plot to make space for the legend\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e63a9fdc-6679-4ea0-ade6-786a33b23af8",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# plot boxplots of areas over different image files\n",
    "plt.figure(figsize=(8, 6))\n",
    "sns.boxplot(data=complete_results, x='area_nucleoplasm', hue='filename')\n",
    "plt.title('Boxplots of nucleoplasm area per image')\n",
    "plt.xlabel('Nucleoplasm area')\n",
    "plt.grid(True)\n",
    "plt.legend(title='File', bbox_to_anchor=(1.05, 1), loc='upper left', ncol=1)\n",
    "plt.tight_layout(rect=[0, 0, 1, 1])  # Adjust the right side of the plot to make space for the legend\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1626658c-983e-4d36-90f2-c67c9135d910",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "Now we can try to visualize the distribution of nucleopasm area as function of the localization of the marker."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aebc592c-0568-4b2f-9801-b3cfb0823e25",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "For that we will quickly introduce the concept of lambda functions, really useful when playing with pandas dataframes. \n",
    "Python lambda functions are small, anonymous functions that can have multiple arguments but only one expression and do not require a function name or identifier. Lambda functions are like shortcuts, as they use fewer lines of code, typically single-line statements. The synthax is the following:    ```lambda arguments : expression```\n",
    "\n",
    "You can find more detailed explainations [here](https://www.dataquest.io/blog/tutorial-lambda-functions-in-python/#:~:text=A%20lambda%20function%20is%20an,and%20returns%20only%20one%20expression.&text=Note%20that%2C%20unlike%20a%20normal,a%20lambda%20function%20with%20parentheses.).\n",
    "We will use a lambda function here that assigns for each row of the column ```localization``` the string value ```membrane``` or ```nucleoplasm```, depending if the value ```ratio``` of the same row is >1 or <1. Here, the argument ```x``` corresponds to the rows of ```complete_results['ratio']``` column, on which the lambda is applied."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4226043d-6167-45b2-bf20-1a1314d898c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "complete_results['localization'] = complete_results['ratio'].apply(lambda x: 'membrane' if x > 1 else 'nucleoplasm')\n",
    "complete_results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e014930b-480b-4dc2-8ef8-178a5228959f",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# plot violin plots of areas over different protein localizations\n",
    "plt.figure(figsize=(8, 6))\n",
    "sns.violinplot(data=complete_results, x='area_nucleoplasm', hue='localization')\n",
    "plt.title('Violin plots of nucleoplasm area')\n",
    "plt.xlabel('Nucleoplasm area')\n",
    "plt.grid(True)\n",
    "plt.legend(title='Protein localization', bbox_to_anchor=(1.05, 1), loc='upper left', ncol=1)\n",
    "plt.tight_layout(rect=[0, 0, 1, 1])  # Adjust the right side of the plot to make space for the legend\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c4f481f-d224-4c3b-af09-a7f7b150d258",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# plot histograms of areas over different protein localizations\n",
    "plt.figure(figsize=(8, 6))\n",
    "sns.histplot(data=complete_results, x='area_nucleoplasm', hue='localization')\n",
    "plt.title('Histogram of nucleoplasm area')\n",
    "plt.xlabel('Nucleoplasm area')\n",
    "plt.grid(True)\n",
    "plt.tight_layout(rect=[0, 0, 1, 1])  # Adjust the right side of the plot to make space for the legend\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ad13279-1dbc-4b5a-a37d-9a22b8c5d991",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Stats"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b604d78a-99dd-4e51-afcb-186e0bf16037",
   "metadata": {},
   "source": [
    "The ```describe()``` method enables you to quickly get a summary of statistics about your variables. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22020b95-5a80-40b6-a8de-a11cd6758d3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "complete_results.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25fa37a5-0172-4c15-95a4-5ec640b02a71",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "If you want to learn which parameters are correlated with other parameters, you can visualize that using pandas’s corr()."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3292f744-528c-40c0-b7c4-a26a55706ae6",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "columns_to_correlate = complete_results.columns[1:-2] # remove filename column before as it is not numerical, localization and label as it is not relevant\n",
    "complete_results[columns_to_correlate].corr()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80a82817-92bc-4d9c-a3f0-3422e121ff77",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Exporting results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b76ab802-e119-41b2-a814-1de8be39f985",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "To export your dataframes, you have several options. Let's say we want to export ```complete_results``` dataframe as csv file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39138eb3-0f16-4a20-8884-d45094b9850c",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "complete_results.to_csv('complete_results_table.csv', index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16308034-3fde-437d-bb1b-fcb9cf7d249c",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "More infos about function arguments [here](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_csv.html) and [here](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_excel.html#pandas.DataFrame.to_excel)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.15"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
