{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# Spot detection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this section we will speak about spot detection. \n",
    "It is really useful when you don't need to get precise segmentation masks for objects, and you just need to count and localize them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "We can divide the topic into 2 main approaches; local maxima detection and blob detection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import math\n",
    "import matplotlib.pyplot as plt\n",
    "import skimage\n",
    "import skimage.io\n",
    "import skimage.morphology\n",
    "import scipy.ndimage as ndi\n",
    "import stackview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Local maxima detection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "For detecting local maxima, more precisely pixels surrounded by pixels with lower intensity, we can use the [```peak_local_max()```](https://scikit-image.org/docs/stable/api/skimage.feature.html#skimage.feature.peak_local_max) function from scikit-image."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "We start by loading an image for demonstration purposes. We use image set [BBBC007v1](https://bbbc.broadinstitute.org/BBBC007) (Jones et al., Proc. ICCV Workshop on Computer Vision for Biomedical Image Applications, 2005), available from the Broad Bioimage Benchmark Collection [Ljosa et al., Nature Methods, 2012]."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "image = skimage.io.imread(\"images/A9_p7d.tif\")\n",
    "plt.subplots(figsize=(8,8))\n",
    "plt.imshow(image, cmap = 'gray');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "A common preprocessing step before detecting maxima is blurring the image. This makes sense to avoid detecting maxima that are just intensity variations resulting from noise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "preprocessed = skimage.filters.gaussian(image, sigma=2, preserve_range=True) # keeps the original range of values\n",
    "plt.subplots(figsize=(8,8))\n",
    "plt.imshow(preprocessed, cmap = 'gray');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "The ```peak_local_max()``` function allows detecting maxima which have higher intensity than surrounding pixels according to a defined threshold."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "coordinates = skimage.feature.peak_local_max(preprocessed, threshold_abs=5)\n",
    "coordinates.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "And these coordinates can be visualized using matplotlib’s plot function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.subplots(figsize=(8,8))\n",
    "plt.imshow(preprocessed, cmap = 'gray')\n",
    "plt.plot(coordinates[:, 1], coordinates[:, 0], 'r.');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "If there are too many maxima detected, one can modify the results by changing the ```sigma``` parameter of the Gaussian blur above or by changing the ```threshold``` passed to the peak_local_max function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "coordinates_2 = skimage.feature.peak_local_max(preprocessed, threshold_abs=100)\n",
    "plt.subplots(figsize=(8,8))\n",
    "plt.imshow(preprocessed, cmap = 'gray')\n",
    "plt.plot(coordinates_2[:, 1], coordinates_2[:, 0], 'r.');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinates.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinates_2.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Blob detection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "A other common procedure for spot detection is blob detection. It is typically applied with Difference-of-Gaussian (DoG), Laplacian-of-Gaussian (LoG) and Determinant-of-Hessian (DoH) images. In this course we will only talk about DoG and LoG, and we will use scikit-image functions. The advantage of [these methods](https://scikit-image.org/docs/stable/auto_examples/features_detection/plot_blob.html) is that no pre-processing is necessary, it is built-in."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For technial reasons it is important to convert the pixel type of the input image first, [see this discussion](https://forum.image.sc/t/how-to-use-scikit-images-blob-dog/75150) if you want to get more details about it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image = image.astype(float)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "### Difference-of-Gaussian (DoG)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "As we have seen in previous chapters, gaussian filters can be chosen to suppress small structures. But what if we also wish to suppress large structures – so that we can concentrate on detecting or measuring structures with sizes inside a particular range ?\n",
    "\n",
    "Suppose we apply one gaussian filter to reduce small structures. Then we apply a second gaussian filter, bigger than the first, to a duplicate of the original image. This will remove even more structures, while still preserving the largest features in the image.\n",
    "The trick is that, if we subtract this second filtered image from the first, we are left with an image that contains the information that ‘falls between’ the two smoothing scales we used. This process is called difference of gaussians (DoG) filtering. The resulting images are subtracted from each other resulting in an image where objects smaller and larger than a defined size or sigma range are removed. Read more in the documentation of [```blob_dog()```](https://scikit-image.org/docs/stable/api/skimage.feature.html#skimage.feature.blob_dog).\n",
    "\n",
    "\n",
    "<img src=\"illustrations/dog_illustration.png\" alt=\"drawing\" width=\"80%\" class=\"center\"/>\n",
    "\n",
    "<img src=\"illustrations/dog_illustration_2.png\" alt=\"drawing\" width=\"80%\" class=\"center\"/>\n",
    "\n",
    "\n",
    "\n",
    "Image [sources](https://bioimagebook.github.io/chapters/2-processing/4-filters/filters.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinates_dog = skimage.feature.blob_dog(image, min_sigma=5, max_sigma=10, threshold=1)\n",
    "coordinates_dog.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This array contains coordinates in x and y and the sigma corresponding to the maximum. We can extract the list of coordinates and visualize it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplots(figsize=(8,8))\n",
    "plt.imshow(image, cmap = 'gray')\n",
    "plt.plot(coordinates_dog[:, 1], coordinates_dog[:, 0], 'r.');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "### Laplacian-of-Gaussian (LoG)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The appearance of a LoG filter is like an upside-down DoG filter.\n",
    "\n",
    "The LoG technique is a laplacian kernel applied to a gaussian blurred image. \n",
    "The appearance of a LoG filter is like an upside-down DoG filter, but if the resulting image is inverted then the results are comparable.\n",
    "\n",
    "<img src=\"illustrations/log_illustration.png\" alt=\"drawing\" width=\"80%\" class=\"center\"/>\n",
    "\n",
    "In fact, DoG filters are a faster approach of LoG filters. The LoG approach is more accurate and slower. See [```blob_log()```](https://scikit-image.org/docs/stable/api/skimage.feature.html#skimage.feature.blob_log) documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinates_log = skimage.feature.blob_log(image, min_sigma=5, max_sigma=10, num_sigma=10, threshold=1)\n",
    "coordinates_log.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplots(figsize=(8,8))\n",
    "plt.imshow(image, cmap = 'gray')\n",
    "plt.plot(coordinates_log[:, 1], coordinates_log[:, 0], 'r.');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " From this set of coordinates we can create a label image and expand the labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a label image from coordinates\n",
    "label_image = np.zeros_like(image, dtype=int)\n",
    "for i, coord in enumerate(coordinates_dog, 1):\n",
    "    y, x = int(coord[0]), int(coord[1])\n",
    "    label_image[y, x] = i  # Assign different labels to detected objects\n",
    "\n",
    "# Expand labels by a fixed number of pixels\n",
    "expanded_labels = skimage.segmentation.expand_labels(label_image, distance=7)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplots(figsize=(8,8))\n",
    "plt.imshow(image, cmap = 'gray', alpha=0.5)\n",
    "plt.imshow(expanded_labels, alpha=0.5);"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.15"
  },
  "rise": {
   "scroll": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
